﻿using UnityEngine;
using System.Collections;

public class PlayerOneHealth : MonoBehaviour {

	//sets players health to 100
	public static int health = 100;
	
	void OnGUI () 
	{
		//displays health of player on GUI
		GUI.Box (new Rect (Screen.width/20, Screen.height/20,200,25 ),"Player 1 Health: " + health);
	}


}
