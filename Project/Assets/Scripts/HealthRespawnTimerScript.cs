﻿using UnityEngine;
using System.Collections;

public class HealthRespawnTimerScript : MonoBehaviour 
{
	//for use of pickup, true = active, false = not active
	public static bool isActive = true;
	//declares timer
	private float timer = 0.0f;
	//the Health powerup
	public GameObject powerUp;
	
	// Update is called once per frame
	void Start()
	{
		//sets the variable to the game object
        powerUp = GameObject.Find("HealthPowerUp");
	}
	void Update () 
	{
		//if picked up start timer
		if (isActive == false) {
			timer += Time.deltaTime;		
		}

		//when timer reaches 25 respawn health powerup
		//resets timer to 0 when respawned
		if (timer >= 25) 
		{ 
            Debug.Log("hello");
			powerUp.SetActive(true);
			isActive = true;
			timer = 0.0f;
		}
	
	}
}
