﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (CharacterController))]
public class PlayerController2 : MonoBehaviour 
{
	// Handling
	public float rotationSpeed = 450;
	public float walkSpeed = 5;
	public float runSpeed = 8;

	// System
	private Quaternion targetRotation;

	// Components
	private CharacterController controller2;
	private Camera cam2;

	void Start () {
		controller2 = GetComponent<CharacterController>();
		cam2 = Camera.main;
	}
	//joystick controls
	void Update () 
	{
		Vector3 input = new Vector3(Input.GetAxisRaw("joystickLeftRight"),0,Input.GetAxisRaw("joystickUpDown"));
		
		Vector3 input2 = new Vector3 (Input.GetAxisRaw ("joystickAimLeftRight"), 0, Input.GetAxisRaw ("joystickAimUpDown"));
		
		if (input2 != Vector3.zero) 
		{
			targetRotation = Quaternion.LookRotation(input2);
			transform.eulerAngles = Vector3.up * Mathf.MoveTowardsAngle(transform.eulerAngles.y,targetRotation.eulerAngles.y,rotationSpeed * Time.deltaTime);
		}
		
		Vector3 motion = input;
		motion *= (Mathf.Abs(input.x) == 1 && Mathf.Abs(input.z) == 1)?.7f:1;
		motion *= (Input.GetButton("joystickRun"))?runSpeed:walkSpeed;
		motion += Vector3.up * -8;
		
		controller2.Move(motion * Time.deltaTime);
	}




	
}
