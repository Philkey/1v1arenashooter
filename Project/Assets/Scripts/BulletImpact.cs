﻿using UnityEngine;
using System.Collections;

public class BulletImpact : MonoBehaviour 
{
	//when bullet collides with something
	void OnCollisionEnter (Collision col)
	{
		//when bullet collides with wall
		//destroy bullet
		if (col.gameObject.name == "Wall") 
        {
			Destroy(gameObject);
		}

		//when bullet collides with barrel
		//destroy bullet
		if (col.gameObject.name == "Barrel") 
        {
			Destroy (gameObject);
		}

		//when bullet collides with player 1
		//destroy bullet
		//lower player 1 health by 10
		if (col.gameObject.name == "Player 1") 
        {
			Destroy (gameObject);

			if(PlayerOneHealth.health >0)
			{
			    PlayerOneHealth.health -= 10;
			}
		}

		//when bullet collides with player 2
		//destroy bullet
		//lower player 2 health by 10
		if (col.gameObject.name == "Player 2")
        {
			Destroy (gameObject);

			if(PlayerTwoHealth.health >0)
			{
				PlayerTwoHealth.health -= 10;
			}
            	
		}

		//when bullet hits outer walls 
		//destoy bullet
        if (col.gameObject.name =="Out Of Bounds")
        {
            Destroy(gameObject);
        }

	}
}
