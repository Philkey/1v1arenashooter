﻿using UnityEngine;
using System.Collections;

public class SpawnAmmoPad : MonoBehaviour 
{
	//one of the four ammo pads inactive in the scene
	public GameObject Ammopad1;
	//one of the four ammo pads inactive in the scene
	public GameObject Ammopad2;
	//one of the four ammo pads inactive in the scene
	public GameObject Ammopad3;
	//one of the four ammo pads inactive in the scene
	public GameObject Ammopad4;
	//an interger for the random number generator
	private int rnd;
	//declared float for the timer 
	private float timer = 0.0f;
	
	// Update is called once per frame
	void Update () {

		//increase time on the timer
		timer += Time.deltaTime;

		//when timer reaches 10
		if (timer >= 10) 
		{
			//generate random number 1-4
			rnd = Random.Range (1, 5);
			//depending on what number is generated
			//select a different ammo pad to make active
			if (rnd == 1)
			{
				Ammopad1.SetActive(true);

				timer = 0.0f;
			}
			if (rnd == 2)
			{
				Ammopad2.SetActive(true);

				timer = 0.0f;
			}

			if (rnd == 3)
			{
				Ammopad3.SetActive(true);
			
				timer = 0.0f;
			}

			if (rnd == 4)
			{
				Ammopad4.SetActive(true);

				timer = 0.0f;
			}


		}
	}
}
