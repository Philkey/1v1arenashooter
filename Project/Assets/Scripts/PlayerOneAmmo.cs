﻿using UnityEngine;
using System.Collections;

public class PlayerOneAmmo : MonoBehaviour {

	//magazine ammo loaded by stored ammo (max10)
	public static int ammo = 10;

	//ammo stored, loads into magize ammo afer a reload (max 20)
	public static int storedAmmo = 20;

	//all ammo, ammo + stored ammo (max 30)
	private int allAmmo; 

	//displays ammo amount to user on the GUI
	void OnGUI () 
	{
		GUI.Box (new Rect ((Screen.width/20), (Screen.height/20) *18 ,200,25 ),"Player 1 Ammo: " + ammo + "/" + storedAmmo);
	}

	void Update () 
	{
		//if reload button pressed 
		if (Input.GetButtonDown ("Reload"))
		{
			//collects all remaining ammo
			allAmmo = ammo +storedAmmo;
			
			//cant reload if you have no ammo
			if (allAmmo == 0)
			{
				
			}
			
			//if all ammo remaining is less than 10
			else if (allAmmo < 10)
			{
				ammo = allAmmo;
				storedAmmo = 0;
				
			}
			else
			{
				storedAmmo = allAmmo;
				storedAmmo -= 10;
				ammo = 10;
				
			}

		}
		
	}
}
