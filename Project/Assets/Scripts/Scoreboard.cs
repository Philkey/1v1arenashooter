﻿using UnityEngine;
using System.Collections;

public class Scoreboard : MonoBehaviour {

	//timer itself
	public float RoundTimerFloat;
	//interger to display the timer
	public int RoundTimer = 180;
	// boolean for game over scenario
	public static bool GameOver = false;
		
	// Update is called once per frame
	void Update () 
	{
		//start timer at 180 secs
		//minus time from the timer
		RoundTimer = (int)RoundTimerFloat;
		RoundTimerFloat -= Time.deltaTime;	

		//when timer reaches 0 end the game
		if (RoundTimer <= 0) 
		{
			GameOver = true;
		}

		//when players health reaches 0 end the game
		if (PlayerOneHealth.health == 0)
		{
			GameOver = true;
		}

		//when players health reaches 0 end the game
		if (PlayerTwoHealth.health == 0)
		{
			GameOver = true;
		}

		//when game over bool = true change scene
		//reset game over boolean
		//reset round timer
		//reset player one health and ammo
		//reset player two health and ammo
		if (GameOver ==true)
		{
			Application.LoadLevel ("GameOver");
		    GameOver = false;
            RoundTimer = 180;
			PlayerOneHealth.health = 100;
       		PlayerOneAmmo.ammo = 10;
			PlayerOneAmmo.storedAmmo = 20;
			PlayerTwoHealth.health = 100;
			PlayerTwoAmmo.ammo = 10;
			PlayerTwoAmmo.storedAmmo = 20;
        }
	}

	void OnGUI(){
		//if the game is not over display timer
		if (GameOver == false) 
		{
			GUI.Label (new Rect ((Screen.width / 40 * 19), (Screen.height / 20 * 7), 100, 25), "    Time : " + RoundTimer);
		}
	}
}
   