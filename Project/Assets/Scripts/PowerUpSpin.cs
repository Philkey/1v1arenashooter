﻿using UnityEngine;
using System.Collections;

public class PowerUpSpin : MonoBehaviour 
{
	//spins the powerup (gives more effect)
	void Update () 
	{
		transform.Rotate (0, 180 * Time.deltaTime, 0);
	}

	//when player collides with object
	void OnTriggerEnter (Collider col)
	{
		//if player one collides with object
		//give player health
		if (col.gameObject.name == "Player 1") 
		{
			gameObject.SetActive(false);
			if (PlayerOneHealth.health > 60)
				PlayerOneHealth.health = 100;
			else
				PlayerOneHealth.health += 40;
			//makes it so that the respawn script can start the timer to respawn it later and make it active
			HealthRespawnTimerScript.isActive = false; 
		}

		//if player two collides with object
		//give player health
		if (col.gameObject.name == "Player 2") 
		{
			gameObject.SetActive(false);
			if (PlayerTwoHealth.health > 60)
				PlayerTwoHealth.health = 100;
			else
				PlayerTwoHealth.health += 40;

			//makes it so that the respawn script can start the timer to respawn it later and make it active
			HealthRespawnTimerScript.isActive = false; 
		}
	}
}
