﻿using UnityEngine;
using System.Collections;

public class ActivatePad : MonoBehaviour 
{
	//declares the different ammo pads
	public GameObject redPad;
	public GameObject greenPad;
	public GameObject ammoPad;

	//timer settings
	//timerFloat = the actual time player has to stand on timer
	public float timerFloat = 3.5f; 

	//the displayed time that the user sees on the GUI
	public int timer;

	//used to determine which player is standing on the pad
	private bool P1Active = false;
	private bool P2Active = false;


	// Update is called once per frame
	void Update () 
	{
		//sets the timer = to the timerFloat (sets to the nearest second)
		timer = (int)timerFloat;

		//if player one is standing on the pad
		if (P1Active == true) 
		{	
			//lowers the timer
			timerFloat -= Time.deltaTime;

			if (timerFloat <= 0.1) 
			{
				//player collects ammo
				//ammo pad is set inactive(no longer visable)
				//timer is reset to 3.5
				P1Active = false;
				ammoPad.SetActive (false);
				PlayerOneAmmo.ammo = 10;
				PlayerOneAmmo.storedAmmo = 20;
				timerFloat = 3.5f;
				greenPad.SetActive(false);
				redPad.SetActive(true);
			}
		}

		//if player two is standing on the pad
		if (P2Active == true) 
		{
			//lowers the timer
			timerFloat -= Time.deltaTime;

			if (timerFloat <= 0.1) 
			{
				//player collects ammo
				//ammo pad is set inactive(no longer visable)
				//timer is reset to 3.5
				P2Active = false;
				ammoPad.SetActive (false);
				PlayerTwoAmmo.ammo = 10;
				PlayerTwoAmmo.storedAmmo = 20;
				timerFloat = 3.5f;
				greenPad.SetActive(false);
				redPad.SetActive(true);
			}
		}


	}

	void OnGUI()
	{
		//when true display timer to the GUI
		if (P1Active == true) 
		{
			GUI.Box (new Rect ((Screen.width/20 * 5), (Screen.height/20*4),50,25 ),"" + timer);
		}

		//when true display timer to the GUI
		if (P2Active == true) 
		{
			GUI.Box (new Rect ((Screen.width/20 * 15), (Screen.height/20*4),50,25 ),"" + timer);
		}

	}

	//when either of the players collides or stands on the ammo pads
	void OnTriggerEnter (Collider col)
	{
		//player one
		//change pads to the activated colour
		//set player boolean true
		if (col.gameObject.name == "Player 1") 
		{
			greenPad.SetActive(true);
			redPad.SetActive(false);
			P1Active = true;

		}

		//player two
		//change pads to the activated colour
		//set player boolean true
		else if (col.gameObject.name == "Player 2") 
		{
			greenPad.SetActive(true);
			redPad.SetActive(false);
			P2Active = true;
			
		}

	}

	//when either of the players exits one of the ammo pads
	void OnTriggerExit (Collider col)
	{
		//player one
		//change pads to deactivate colour
		//sets player boolean false
		//resets timer
		if (col.gameObject.name == "Player 1") 
		{
			greenPad.SetActive(false);
			redPad.SetActive(true);
			P1Active = false;
			timerFloat = 3.5f;
		}

		//player one
		//change pads to deactivate colour
		//sets player boolean false
		//resets timer
		if (col.gameObject.name == "Player 2") 
		{
			greenPad.SetActive(false);
			redPad.SetActive(true);
			P2Active = false;
			timerFloat = 3.5f;

			
		}
	}
}
