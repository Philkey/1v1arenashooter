﻿using UnityEngine;
using System.Collections;

public class GameCamera2 : MonoBehaviour 
{
	//camera position
	private Vector3 cameraTwoPosition;
	//players position
	private Transform playerTwoPosition;
	
	void Start () 
	{
		//sets the player position to the variable
		playerTwoPosition = GameObject.FindGameObjectWithTag("Player2").transform;
	}
	
	void Update () 
	{
		//updates the cameras position to the players position
		cameraTwoPosition = new Vector3(playerTwoPosition.position.x,transform.position.y,playerTwoPosition.position.z);
		transform.position = Vector3.Lerp(transform.position,cameraTwoPosition,Time.deltaTime * 8);
	}
}
