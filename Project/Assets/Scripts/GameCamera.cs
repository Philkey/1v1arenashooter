﻿using UnityEngine;
using System.Collections;

public class GameCamera : MonoBehaviour 
{
	//camera position
	private Vector3 cameraOnePosition;
	//players position
	private Transform playerOnePosition;
	
	void Start () 
	{
		//sets the player position to the variable
		playerOnePosition = GameObject.FindGameObjectWithTag("Player").transform;
	}

	void Update () 
	{
		//updates the cameras position to the players position
		cameraOnePosition = new Vector3(playerOnePosition.position.x,transform.position.y,playerOnePosition.position.z);
		transform.position = Vector3.Lerp(transform.position,cameraOnePosition,Time.deltaTime * 8);
	}
}
