﻿using UnityEngine;
using System.Collections;

public class GunShoot2 : MonoBehaviour 
{
	//sets the bullet rigidbody
	public Rigidbody projectile;
	//sets the bullet speed
	public float speed = 20;

	// Update is called once per frame
	void Update () {

		//if player has ammo
		if (PlayerTwoAmmo.ammo > 0)
		{
			//if fire button pressed
			//fire projectile from source (gameobject with script attached)
			//firre projectile with velociy = speed 
			//lower players ammo by 1
			if (Input.GetButtonDown ("joystickFire")) 
			{
				Rigidbody instantiatedProjectile = Instantiate(projectile,transform.position,transform.rotation)as Rigidbody;
				instantiatedProjectile.velocity = transform.TransformDirection (new Vector3 (0, 0, speed));
				PlayerTwoAmmo.ammo -= 1;
			}
		}
	}
}
