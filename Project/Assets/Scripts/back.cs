﻿using UnityEngine;
using System.Collections;

public class back : MonoBehaviour 
{
	//back button to game over screen
	void OnGUI()
	{
		//if button clicked
		if (GUI.Button (new Rect ((Screen.width / 40 * 18), (Screen.height / 20 * 15), 100, 25), "Back to Menu"))
		{
			//load main menu scene
			Application.LoadLevel("Main Menu");
		}
	}
}
