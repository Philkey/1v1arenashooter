﻿using UnityEngine;
using System.Collections;

public class PlayerTwoHealth : MonoBehaviour {

	//sets players health to 100
	public static int health = 100;
	
	void OnGUI () 
	{
		//displays health of player on GUI		
		GUI.Box (new Rect ((Screen.width/20)*11, Screen.height/20,200,25 ),"Player 2 Health: " + health);
	}
}
